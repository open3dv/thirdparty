/*
 * Copyright 2016, Simula Research Laboratory
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#ifndef CCTAG_SERIALIZE
/* #undef CCTAG_SERIALIZE */
#endif

#ifndef CCTAG_VISUAL_DEBUG
/* #undef CCTAG_VISUAL_DEBUG */
#endif

#ifndef CCTAG_NO_COUT
#define CCTAG_NO_COUT
#endif

#ifndef CCTAG_WITH_CUDA
/* #undef CCTAG_WITH_CUDA */
#endif

#ifdef CCTAG_WITH_CUDA

  #ifndef CCTAG_HAVE_SHFL_DOWN_SYNC
/* #undef CCTAG_HAVE_SHFL_DOWN_SYNC */
  #endif

  #ifndef CCTAG_NO_THRUST_COPY_IF
/* #undef CCTAG_NO_THRUST_COPY_IF */
  #endif

#endif
