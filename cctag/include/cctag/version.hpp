/*
 * Copyright 2016, Simula Research Laboratory
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#define CCTAG_VERSION_MAJOR 1
#define CCTAG_VERSION_MINOR 0
#define CCTAG_VERSION_PATCH 3

#define CCTAG_VERSION_STRING "1.0.3"
